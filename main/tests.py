from django.test import LiveServerTestCase, TestCase, tag
from django.urls import resolve
from django.test import TestCase , Client
from django.apps import apps
from .apps import MainConfig
from .views import home 


class UnitTestku(TestCase):

    def test_status(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_apps(self):
        self.assertEqual(MainConfig.name, 'main')
        self.assertEqual(apps.get_app_config('main').name, 'main') 

    def test_get(self):
            response = Client().get('/')
            html_kembalian = response.content.decode('utf8')
            self.assertIn("Kegiatan saat ini", html_kembalian)
            self.assertIn("Pengalaman organisasi", html_kembalian)   
            self.assertIn("Prestasi", html_kembalian)
            self.assertIn("Game Favorite", html_kembalian)   
           
    def test_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)